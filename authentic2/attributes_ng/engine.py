from django.utils.importlib import import_module
from django.core.exceptions import ImproperlyConfigured

from ..decorators import to_list
from .. import app_settings

__ALL__ = ['get_attribute_names', 'get_attributes']

class UnsortableError(Exception):
    '''
    Raise when topological_sort is unable to sort instance topologically.
    sorted_list contains the instances that could be sorted unsorted contains
    the instances that couldn't.
    '''
    def __init__(self, sorted_list, unsortable_instances):
        self.sorted_list = sorted_list
        self.unsortable_instances = unsortable_instances

    def __str__(self):
        return 'UnsortableError: %r' % self.unsortable_instances

def topological_sort(source_and_instances, ctx):
    '''
    Sort instances topologically based on their dependency declarations.
    '''
    sorted_list = []
    variables = set(ctx.keys())
    unsorted = list(source_and_instances)
    while True:
        count_sorted = len(sorted_list)
        new_unsorted = []
        for source, instance in unsorted:
            dependencies = set(source.get_dependencies(instance, ctx))
            if dependencies <= variables:
                sorted_list.append((source, instance))
                variables.update(a for a, b in source.get_attribute_names(instance, ctx))
            else:
                new_unsorted.append((source, instance))
        unsorted = new_unsorted
        if len(sorted_list) == len(source_and_instances): # finished !
            break
        elif count_sorted == len(sorted_list): # no progress !
            raise UnsortableError(sorted_list, unsorted)
    return sorted_list

@to_list
def get_sources():
    '''
    List all known sources
    '''
    for path in app_settings.ATTRIBUTE_BACKENDS:
        try:
            source = import_module(path)
        except ImportError:
            try:
                module, attr = path.rsplit('.', 1)
                source = import_module(module)
                source = getattr(source, attr)
            except (ImportError, AttributeError):
                raise ImproperlyConfigured('unable to import attribute backend: %r' % path)
        yield source

@to_list
def get_attribute_names(ctx):
    '''
    Return attribute names from all sources
    '''
    for source in get_sources():
        for instance in source.get_instances(ctx):
            for attribute_name, attribute_description in source.get_attribute_names(instance, ctx):
                yield attribute_name, attribute_description


def get_attributes(ctx):
    '''
    Traverse and sources instances and aggregate produced attributes.

    Traversal is done by respecting a topological sort of instances based on
    their declared dependencies
    '''
    source_and_instances = []
    for source in get_sources():
        source_and_instances.extend(((source, instance) for instance in
            source.get_instances(ctx)))
    source_and_instances = topological_sort(source_and_instances, ctx)
    ctx = ctx.copy()
    for source, instance in source_and_instances:
        ctx.update(source.get_attributes(instance, ctx.copy()))
    return ctx
