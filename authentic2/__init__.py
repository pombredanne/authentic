import sys
import os

__version__ = "2.1.9"

# vendor contains incorporated dependencies
sys.path.append(os.path.join(os.path.dirname(__file__), 'vendor'))
